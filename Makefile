SHELL := /bin/bash -eo pipefail
MAKEFLAGS := --jobs=1

# import config.
# You can change the default config with `make cnf="config_special.env" build`
cnf ?= config.env
include $(cnf)
export $(shell sed 's/=.*//' $(cnf))

# Overwrite APP_VERSION if defined at cli
APP_VERSION := ${APP_VERSION}

.PHONY: local-requisites remote-requisites deploy update_app help destroy

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

deploy: local-requisites remote-requisites ## Start the Guestbook Application local or remote - use DOCKER_HOST to run the application on a remote Docker Host
	@# TODO - Allow to override entrypoint to run on OSX
	@cd ./docker ; \
	docker-compose up -d

logs: local-requisites remote-requisites ## Stream the application logs
	@cd ./docker ; \
	docker-compose logs -f

clean_images: local-requisites remote-requisites ## Remove all images tagges for guestbook-webapp
	docker rmi `docker images --filter label="app_name=guestbook-webapp" -q` 2>/dev/null || exit 0

status: local-requisites remote-requisites ## Show the application stack status
	@cd ./docker ; \
	docker-compose ps

update_app: local-requisites remote-requisites ## Update Guestbook APP Container by restarting it using an updated APP_VERSION
	@# TODO - Allow to override entrypoint to run on OSX
	@cd ./docker ; \
	docker-compose pull guestbook && docker-compose rm --force --stop guestbook && docker-compose up --no-deps -d guestbook

destroy: local-requisites remote-requisites ## Destroy the running Application - Persistent Data will not be removed
	@cd ./docker ; \
	docker-compose down

local-requisites: ## Check that the required tools are available on this host
	@cd ./scripts ; \
	./check-local-req.sh

remote-requisites: ## Check that the required variables for remote execution are set
	@cd ./scripts ; \
	./check-remote-req.sh
