# guestbook

Guestbook Project

## QuickStart

* Run the following commands
```
git clone git@gitlab.com:primeroznl/guestbook.git
cd guestbook
make deploy
make status
```

* open browser to http://localhost/

## Description

This is a sample repository to deploy a full stack of the [Guestbook](https://github.com/jaxxstorm/example-webapp-go/blob/master/main.go) application

The Application uses a Redis database to store and retrieve simple Guestbook messages

```
+--------------+                   +-----------------+
|              |     HTTP          |                 |
|  Client      +------------------>+   Guestbook App |
|              |     Web Browser   |                 |
+--------------+                   +-------+---------+
                                           |
                                           |
                                           |
                                           |
                                   +-------v---------+
                                   |                 |
                                   |   REDIS         |
                                   |                 |
                                   +-----------------+

```

## Supported Platforms

At the moment only Linux is supported. 
We could easily add support for OSX if requested

## Requirements

* [make](https://www.gnu.org/software/make/)
  * make is used to automate the different lifecycle stages of the application
* [docker-compose](https://docs.docker.com/compose/)
  * docker-compose is used to deploy and manage the application through docker engine
* [docker](https://www.docker.com/)
  * docker engine is used to run the containers

## Usage

### Building the guestbook container image

The Guestbook container image is built by the *gitlab CI pipeline* included with this repo.
The application source code is not part of this repo , instead of a static tarball is made available from the upstream vendor.

**NOTE that creating a new version of the docker image through this pipeline won't necessarily update the Application Version which is bundled from upstream**

To create a new Docker image through this pipeline simply push a new *branch* or *tag* to this repo. 
Currently the **master** branch is considered stable and is the only branch from which an image tagged *latest* will be created

#### Image Testing

During the test stage of the Pipeline the application is tested for the following :

* The Application can server the static assets successfully
* Client can write to the database through the Application
* Client can read from the database through the Application

### Application Deployment

The Application deployment lifecycle is managed through *make* and *docker-compose*

It support both LOCAL and REMOTE Deployment, currently **ONLY one deployment per host is supported**

The deployment configuration is managed through an external file called *config.env* , 
you can either overwrite the settings of this file to fit your requirments or create a new file and specify its location when running the make command

#### Local Deployment

* Edit the *config.env* to make sure that all parameters are correct
  * Make sure that the *DOCKER_HOST, DOCKER_TLS, DOCKER_TLS_VERIFY, DOCKER_CERT_PATH* parameters are all commented out
  * Make sure that *APP_VERSION* is set to the version you want to deploy. 
* Run ***make status*** to check the status of the deployment before proceeding
* Run ***make deploy*** to deploy your application
* Open your browser at http://localhost/

#### Remote Deployment

* Edit the *config.env* to make sure that all parameters are correct
  * Make sure that the *DOCKER_HOST, DOCKER_TLS, DOCKER_TLS_VERIFY, DOCKER_CERT_PATH* parameters are all un-commented and set to the correct values
    * *DOCKER_HOST* should point to your remote Docker host as *tcp://your.docker.host:2376*
    * Ideally you will be using TLS , make sure *DOCKER_CERT_PATH* is set to the directory containing your *ca.pem, cert.pem, key.pem* files to connect to the remote docker host
  * Make sure that *APP_VERSION* is set to the version you want to deploy. 
* Run ***make status*** to check the status of the deployment before proceeding
* Run ***make deploy*** to deploy your application
* Open your browser at http://your.docker.host/

#### Update a running application to a new version of the guestbook image

When a new version of the image is created you can use *make* and *docker-compose* to update your running stack to the latest version of the application.
Assuming your new image is tagged as *:latest* you can simply run

* Run ***make update_app*** to force an update of the running guestbook container to the latest application

#### Specify a custom version of the guestbook application to run

You can specify a custom version of the image to run for the guestbook application , probably for testing purposes, by runnig

* Run ***make deploy APP_VERSION=$(IMAGE_REFERENCE)*** or ***make update_app APP_VERSION=$(IMAGE_REFERENCE)***

#### Use multiple configuration files

It is possible to have multiple configuration files with different settings to mantain multiple environments and make it easy to switch between them

* Copy config.env to a new file *dev.env* , *test.env* and *prod.env*
* Edit each file accordingly , make sure to change the *DOCKER_HOST* 
* Run *make deploy cnf=(dev,env,prod).env*

## Known Issues

### Pipeline for guestbook application is not ideal

Ideally the Pipeline to create , and properly tag, the guestbook application would be in a separate repo where the guestbook application code lives.
This would let us tag the image accordingly to the version of the code.

Also with the current setup any change to this repo, probably unrelated to the docker image itself, will create a new version of the guestbook container image.

### Can't deploy multiple stack to a single host

Currently only one single stack can be deployed to a single host.

By parameterizing more of the docker-compose service definition we could deploy multiple stack to a single host and simplify testing of new releases
