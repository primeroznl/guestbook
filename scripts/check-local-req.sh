#!/bin/bash
set -e

# Check that all the pre-requisite tools are available , if not exit and ask user to install them before proceeding
#
# * docker-compose
# * curl
# * sha256sum


source ./support.sh

function check_tool_is_available {
  TOOL=$1
  
  if ! (command -v $TOOL 2>&1 >/dev/null); then
    e_error "$TOOL command is not available in the User PATH"
    echo ""
    e_arrow "Please install $TOOL to proceed with deployment"
    exit 1
  fi
}


check_tool_is_available "docker-compose"
check_tool_is_available "curl"
check_tool_is_available "sha256sum"
