#!/bin/bash
set -e

source ./support.sh

[[ ! -z ${DOCKER_HOST} ]] && (e_underline "Running a remote deployment - Remote Docker Host: ${DOCKER_HOST}")
[[ ! -z ${DOCKER_HOST} ]] && [[ -z ${DOCKER_TLS} ]] && (e_warning "DOCKER_TLS is not set , docker communication will be unencrypted")
[[ ! -z ${DOCKER_HOST} ]] && [[ -z ${DOCKER_TLS_VERIFY} ]] && [[ ! -z ${DOCKER_TLS} ]] && (e_warning "DOCKER_TLS_VERIFY is not set , docker will not verify the validity of the TLS certificates")
[[ ! -z ${DOCKER_HOST} ]] && [[ -z ${DOCKER_CERT_PATH} ]] && [[ ! -z ${DOCKER_TLS} ]] && (e_warning "DOCKER_CERT_PATH is not set , docker will search for tls assets in ~/.docker/{ca,cert,key}.pem")

exit 0
