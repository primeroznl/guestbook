#!/bin/bash
set -x
set -e
set -o pipefail

# Check that / return 200
echo "Testing that http://guestbook:3000/ return 200"
curl -q -f http://guestbook:3000/ 2>/dev/null >/dev/null

echo "Testing that database connectivity Works"
# Create Checksum file for expected output
cat > /tmp/sha256sum.txt << EOF
f189e5e53527d25cf1387dc25c7dee1056ac0f586ae9a73698d3b6d04fe7e994  -
EOF

# Populate Guestbook with expected Entry "Satoshi Nakamoto"
echo "Writing to Database"
curl -q -f http://guestbook:3000/rpush/guestbook/Satoshi%20Nakamoto 2>/dev/null

# Test Output from guestbook application
echo "Reading from Database"
curl -f -q http://guestbook:3000/lrange/guestbook 2>/dev/null | sha256sum -c /tmp/sha256sum.txt
